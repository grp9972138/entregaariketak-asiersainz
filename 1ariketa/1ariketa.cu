#include <stdio.h>
#include <cuda_runtime.h>

int main() {
  // Gailuaren informazioa gordetzeko aldagaia
  cudaDeviceProp prop;

  // Gailuaren id-a
  int deviceId = 0;

  // Gailuaren ezaugarriak lortu
  cudaGetDeviceProperties(&prop, deviceId);

  // Gailuaren informazioa eman
  printf("Gailuaren izena: %s\n", prop.name);
  printf("Multiprozesadore zenbakia: %d\n", prop.multiProcessorCount);
  printf("Konputazio Ahalmena: %d.%d\n", prop.major, prop.minor);
  printf("Memoria orokor totala: %zu bytes\n", prop.totalGlobalMem);
  printf("Bloke bakoitzeko hari kopuru maximoa: %d\n", prop.maxThreadsPerBlock);
  printf("Multiprozesadore bakoitzeko hari kopuru maximoa: %d\n", prop.maxThreadsPerMultiProcessor);

  return 0;
}
