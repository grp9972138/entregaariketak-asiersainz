#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void gen_matrixes(int a, int b, int c, float **A, float **B, float **C){

	// Matrizeentzako memoria erreserbatu
  *A=(float *)malloc(a*b*sizeof(float));

  *B=(float *)malloc(b*c*sizeof(float));

  *C=(float *)malloc(a*c*sizeof(float));

	// Matrizeak hasieratu ausazko balioekin
  
  srand(time(NULL)); // Ziurtatzeko programa exekutazten den bakoitzean balio desberdinak izango ditugula
  for(int i=0; i<(a*b); i++){
    (*A)[i]=rand()%5;
  }
  for(int i=0; i<b*c; i++){
    (*B)[i]=rand()%5;
  }
  for(int i=0; i<a*c; i++){
		(*C)[i]=rand()%5;
  }
}
void print_matrixes(int a, int b, int c, float **A, float **B, float **C){

	// A matrizea
	printf("\nA matrizea:\n");
  for (int i = 0; i < a; i++) {
     for (int j = 0; j < b; j++) {
       printf("%f ", (*A)[i * b + j]);
     }
     printf("\n");
  }

  // B matrizea
	printf("\nB matrizea:\n");
  for (int i = 0; i < b; i++) {
     for (int j = 0; j < c; j++) {
       printf("%f ", (*B)[i * c + j]);
     }
     printf("\n");
  }

  // C matrizea
  printf("\nC matrizea:\n");
  for (int i = 0; i < a; i++) {
     for (int j = 0; j < c; j++) {
       printf("%f ", (*C)[i * c + j]);
     }
     printf("\n");
  }

}

int main(int argc, char *argv[]){

	// Matrizeen bektoreak deklaratu
	float *mA, *mB, *mC;

	// Jasotako argumentuak pasa gen_matrixes funtzioari
	int mAmC_tamaina_X = strtoul(argv[1], NULL, 10);

	int mAmB_tamaina_YX = strtoul(argv[2], NULL, 10);

	int mBmC_tamaina_Y = strtoul(argv[3], NULL, 10);

  // Exekuzio denborak kalkulatzeko
  clock_t hasi, bukatu;
  double exek_denb;

	gen_matrixes(mAmC_tamaina_X, mAmB_tamaina_YX, mBmC_tamaina_Y, &mA, &mB, &mC);
  
  printf("[+] HASIERAKO MATRIZEAK\n");
	print_matrixes(mAmC_tamaina_X, mAmB_tamaina_YX, mBmC_tamaina_Y, &mA, &mB, &mC);
  
  // Behin matrizeak hasieratuta zenbatzen hasi
  hasi=clock();

	int Z=mAmC_tamaina_X*mBmC_tamaina_Y;

  for(int i=0; i<mAmC_tamaina_X; i++){
    for(int j=0; j<mBmC_tamaina_Y; j++){
      for(int k=0; k<mAmB_tamaina_YX; k++){
        mC[i*mBmC_tamaina_Y+j]+=mA[i*mAmB_tamaina_YX+k] * mB[k*mBmC_tamaina_Y+j];
      }
    }
  }

  // Denbora zenbatzen gelditu
  bukatu=clock();
  // Kalkulatu denbora exekuzioa
  exek_denb = ((double) (bukatu - hasi)) / CLOCKS_PER_SEC;

  printf("[+] BUKAERAKO MATRIZEAK\n");
	print_matrixes(mAmC_tamaina_X, mAmB_tamaina_YX, mBmC_tamaina_Y, &mA, &mB, &mC);
  printf("\n");

  printf("EXEKUZIO DENBORA->%f\n", exek_denb);

	free(mA);
	free(mB);
	free(mC);
	return 0;
}
