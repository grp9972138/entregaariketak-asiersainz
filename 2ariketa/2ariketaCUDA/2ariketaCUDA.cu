#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <cuda_runtime_api.h>

void gen_matrixes(int a, int b, int c, float **A, float **B, float **C)
{
	// Matrizeentzako memoria erreserbatu CPU-an
  *A=(float *)malloc(a*b*sizeof(float));

  *B=(float*)malloc(b*c*sizeof(float));

  *C=(float*)malloc(a*c*sizeof(float));
  
	// Matrizeak hasieratu ausazko balioekin
  srand(time(NULL)); // Ziurtatzeko programa exekutazten den bakoitzean balio desberdinak izango ditugula
  for(int i=0; i<(a*b); i++){
    (*A)[i]=rand()%5;
  }
  for(int i=0; i<b*c; i++){
    (*B)[i]=rand()%5;
  }
  for(int i=0; i<a*c; i++){
		(*C)[i]=rand()%5;
  }
}

void print_matrixes(int a, int b, int c, float **A, float **B, float **C){

	// A matrizea
	printf("\nA matrizea:\n");
  for (int i = 0; i < a; i++) {
     for (int j = 0; j < b; j++) {
       printf("%f ", (*A)[i * b + j]);
     }
     printf("\n");
  }

  // B matrizea
	printf("\nB matrizea:\n");
  for (int i = 0; i < b; i++) {
     for (int j = 0; j < c; j++) {
       printf("%f ", (*B)[i * c + j]);
     }
     printf("\n");
  }

  // C matrizea
  printf("\nC matrizea:\n");
  for (int i = 0; i < a; i++) {
     for (int j = 0; j < c; j++) {
       printf("%f ", (*C)[i * c + j]);
     }
     printf("\n");
  }
  printf("\n");
}

__global__ void eragiketa (float *A, float *B, float *C, unsigned int M)
{
  unsigned int id = blockIdx.x * blockDim.x + threadIdx.x;
  unsigned int zutabea=id%M;
  unsigned int errenkada=id/M;
  unsigned int i;

  for(i=0; i<M; i++){
      C[errenkada*M+zutabea]+=A[errenkada*M+i]*B[i*M+zutabea];
  }
}

int main(int argc, char *argv[]){
  // Datu bilketa
  int numBlocks=64;
  int threadsPerBlock=32;

  printf("[+] Kerneleko bloke zenbakia adierazi(64<=>1):\n");
  scanf("%d", &numBlocks);
  while(numBlocks>64 || numBlocks<=0){
    printf("[!] Balio ez onartua\n");
    printf("[+] Beste balio bat sartu:\n");
    scanf("%d", &numBlocks);
  }

  printf("[+] Kerneleko bloke bakoitzeko hari zenbakia adierazi(32<=>1):\n");
  scanf("%d", &threadsPerBlock);
  while(threadsPerBlock>32 || threadsPerBlock<=0){
    printf("[!] Balio ez onartua\n");
    printf("[+] Beste balio bat sartu:\n");
    scanf("%d", &threadsPerBlock);
  }

	// Matrizeen bektoreak eta pointer-ak deklaratu
	float *mA, *mB, *mC, *m_A, *m_B, *m_C;

	// Jasotako argumentuak pasa gen_matrixes funtzioari
	int mAmC_tamaina_X = strtoul(argv[1], NULL, 10);
  int a = mAmC_tamaina_X; 
	int mAmB_tamaina_YX = strtoul(argv[2], NULL, 10);
  int b = mAmB_tamaina_YX;
	int mBmC_tamaina_Y = strtoul(argv[3], NULL, 10);
  int c = mBmC_tamaina_Y;
  
  // Matrizeentzako memoria erreserbatu GPU-an
  cudaMalloc(&m_A, a*b*sizeof(float));
  cudaMalloc(&m_B, b*c*sizeof(float));
  cudaMalloc(&m_C, a*c*sizeof(float));
  
  // Exekuzio denbora kalkulatzeko
  cudaEvent_t hasi, bukatu;
  float gpu_denbora;

  // Hasieratu aldagaiak egin
  cudaEventCreate(&hasi);
  cudaEventCreate(&bukatu);

  // HOST-ean matrizeak hasieratu
	gen_matrixes(mAmC_tamaina_X, mAmB_tamaina_YX, mBmC_tamaina_Y, &mA, &mB, &mC);
  
  // Matzizeak GPU-ra eraman
  cudaMemcpy(m_A, mA, a*b*sizeof(float), cudaMemcpyHostToDevice);
  cudaMemcpy(m_B, mB, b*c*sizeof(float), cudaMemcpyHostToDevice);
  cudaMemcpy(m_C, mC, a*c*sizeof(float), cudaMemcpyHostToDevice);

  printf("[+] HASIERAKO MATRIZEAK\n");
	//print_matrixes(a, b, c, &mA, &mB, &mC);

  //Denbora neurtzen hasi
  cudaEventRecord(hasi);

  // Kernela deitu
  eragiketa<<<numBlocks,threadsPerBlock>>>(m_A, m_B, m_C, mAmB_tamaina_YX);

  // Gelditu denbora neurketa
  cudaEventRecord(bukatu);
  cudaEventSynchronize(bukatu);
  
  cudaDeviceSynchronize();
  cudaMemcpy(mC, m_C, (a*c)*sizeof(float), cudaMemcpyDeviceToHost); // Honek datuak GPU-tik HOST-era ekartzen ditu

  //Kalkulatu denbora
  cudaEventElapsedTime(&gpu_denbora, hasi, bukatu);

  printf("[+] BUKAERAKO MATRIZEAK\n");
	//print_matrixes(a, b, c, &mA, &mB, &mC);

  printf("BLOKE ZENBAKIA: %d | HARI ZENBAKIA BLOKEKO: %d }-> EXEKUZIO DENBORA: %f\n", numBlocks, threadsPerBlock, gpu_denbora);

  // Memoria askatu
	free(mA);
	free(mB);
	free(mC);

  cudaFree(m_A);
  cudaFree(m_B);
  cudaFree(m_C);

  // Aldagaiak askatu
  cudaEventDestroy(hasi);
  cudaEventDestroy(bukatu);

	return 0;
}
