#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <cuda_runtime_api.h>
#include <cuda.h>

#define BLOKE_TAM 32
#define THREADS_PER_BLOCK 32

void gen_matrixes(int a, int b, int c, float **A, float **B, float **C, int resize_a, int resize_b, int resize_c)
{
        //printf("GEN MATRIX PARAMETROAK--> a=%d | b=%d | c=%d | r_a=%d | r_b=%d | r_c=%d\n", a, b, c, resize_a, resize_b, resize_c);
        // Matrizeentzako memoria erreserbatu CPU-an
  *A=(float *)malloc(a*b*sizeof(float));

  *B=(float*)malloc(b*c*sizeof(float));

  *C=(float*)malloc(a*c*sizeof(float));
  
  if(resize_b==0 && resize_a==0 && resize_c==0){
          // Matrizeak hasieratu ausazko balioekin
    srand(time(NULL)); // Ziurtatzeko programa exekutazten den bakoitzean balio desberdinak izango ditugula
    for(int i=0; i<(a*b); i++){
      (*A)[i]=rand()%5;
    }
    for(int i=0; i<b*c; i++){
      (*B)[i]=rand()%5;
    }
    for(int i=0; i<a*c; i++){
                  (*C)[i]=rand()%5;
    }
  }else{
          // Matrizeak hasieratu ausazko balioekin
    srand(time(NULL)); // Ziurtatzeko programa exekutazten den bakoitzean balio desberdinak izango ditugula
        int pos_a_zbk= ((a-resize_a)*(b-resize_b));
        int pos_b_zbk= ((b-resize_b)*(c-resize_c));
        int pos_c_zbk= ((a-resize_a)*(c-resize_c));
    //printf("A %d posizio arte bete da\n", pos_a_zbk);
    for(int i=0; i<pos_a_zbk; i++){
      (*A)[i]=rand()%5;
    }
    //printf("B %d posizio arte bete da\n", pos_b_zbk);
    for(int i=0; i<pos_b_zbk; i++){
      (*B)[i]=rand()%5;
    }
    //printf("C %d posizio arte bete da\n", pos_c_zbk);
    for(int i=0; i<pos_c_zbk; i++){
                  (*C)[i]=rand()%5;
    }
    // 0 bete
    int pos_a_0_zbk=pos_a_zbk-1;
    int pos_b_0_zbk=pos_b_zbk-1;
    int pos_c_0_zbk=pos_c_zbk-1;
    for(int i=pos_a_0_zbk; i<a*b; i++){
      (*A)[i]=0;
    }
    for(int i=pos_b_0_zbk; i<b*c; i++){
      (*B)[i]=0;
    }
    for(int i=pos_c_0_zbk; i<a*c; i++){
                  (*C)[i]=0;
    }
  }
}

void print_matrixes(int a, int b, int c, float **A, float **B, float **C){

        // A matrizea
        printf("\nA matrizea:\n");
  for (int i = 0; i < a; i++) {
     for (int j = 0; j < b; j++) {
       printf("%f ", (*A)[i * b + j]);
       }
     printf("\n");
  }

  // B matrizea
        printf("\nB matrizea:\n");
  for (int i = 0; i < b; i++) {
     for (int j = 0; j < c; j++) {
       printf("%f ", (*B)[i * c + j]);
     }
     printf("\n");
  }

  // C matrizea
  printf("\nC matrizea:\n");
  for (int i = 0; i < a; i++) {
     for (int j = 0; j < c; j++) {
       printf("%f ", (*C)[i * c + j]);
     }
     printf("\n");
  }
  printf("\n");
}

__global__ void eragiketa(float* A, float* B, float* C, int a, int b, int c) {

  __shared__ float As[BLOKE_TAM][BLOKE_TAM], Bs[BLOKE_TAM][BLOKE_TAM];  // GPU memoria partekatuan azpimatrizeak

  // Blokeen posizioa
  int bloke_x = blockIdx.x;
  int bloke_y = blockIdx.y;

  // Matrize osoan
  int m_col = threadIdx.x;
  int m_row = threadIdx.y;

  // Bloke barnean
  int bloke_row = bloke_y * blockDim.y + m_row;
  int bloke_col = bloke_x * blockDim.x + m_col;

  float biderketa = 0.0;

  for (int m = 0; m < (b + BLOKE_TAM - 1) / BLOKE_TAM; ++m) {
    if (bloke_row<a && m*BLOKE_TAM+m_col<b) {
      As[m_row][m_col] = A[bloke_row*b + m*BLOKE_TAM + m_col];
    } else {
      As[m_row][m_col] = 0.0;
    }
    if (m*BLOKE_TAM+m_col < b && bloke_row < c) {
      Bs[m_row][m_col] = B[(bloke_y * BLOKE_TAM + m_row) * c + m * BLOKE_TAM + m_col];
    } else {
      Bs[m_row][m_col] = 0.0;
    }
    __syncthreads();
    for (int i = 0; i < BLOKE_TAM; ++i) {
      biderketa += As[m_row][i] * Bs[m_col][i]; // A eta B azpimatrizeen arteko biderketa
    }
    __syncthreads();
  }

  if (bloke_row < a && bloke_col < c) {
    C[bloke_row*c+bloke_col] += biderketa;  // Biderketaren balioa gehitu C matrizeari
  }
}

int bi_berredura(int zbk)
{
 if (zbk == 0) {
    return 0;
  }
  return (zbk & (zbk - 1)) == 0;
  }

int bi_berredura_hurbilena(int zbk)
{
  int potentzia = 1;
  while (potentzia < zbk) {
    potentzia *= 2;
  }
  return potentzia;
}

int main(int argc, char *argv[]){

        // Matrizeen bektoreak eta pointer-ak deklaratu
        float *mA, *mB, *mC, *m_A, *m_B, *m_C;

        // Jasotako argumentuak pasa gen_matrixes funtzioari
        int mAmC_tamaina_X = strtoul(argv[1], NULL, 10);
  int a = mAmC_tamaina_X; 
        int mAmB_tamaina_YX = strtoul(argv[2], NULL, 10);
  int b = mAmB_tamaina_YX;
        int mBmC_tamaina_Y = strtoul(argv[3], NULL, 10);
  int c = mBmC_tamaina_Y;

  int* resize_abc = (int*)malloc(3*sizeof(int));
  resize_abc[0]=a;
  resize_abc[1]=b;
  resize_abc[2]=c;
  for(int i=0; i<3; i++){
    if(bi_berredura(resize_abc[i])){
       resize_abc[i]=0;
    }else{
      int ber_hurbil=bi_berredura_hurbilena(resize_abc[i]);
      //printf("Potentzia hurbilena %d-rentzako %d da\n", i, ber_hurbil);
      resize_abc[i]=ber_hurbil-resize_abc[i];
      if(i==0){
        a=ber_hurbil;
      }else if(i==1){
        b=ber_hurbil;
      }else{
        c=ber_hurbil;
      }
    }
  }
  // Bloke kopurua kalkulatu
  int numBlocks=a*c/(THREADS_PER_BLOCK*THREADS_PER_BLOCK);
  numBlocks = sqrt(numBlocks);

  // Matrizeentzako memoria erreserbatu GPU-an
  cudaMalloc(&m_A, a*b*sizeof(float));
  cudaMalloc(&m_B, b*c*sizeof(float));
  cudaMalloc(&m_C, a*c*sizeof(float));
  
  // Exekuzio denbora kalkulatzeko
  cudaEvent_t hasi, bukatu;
  float gpu_denbora;

  // Hasieratu aldagaiak egin
  cudaEventCreate(&hasi);
  cudaEventCreate(&bukatu);

  // HOST-ean matrizeak hasieratu
        gen_matrixes(a, b, c, &mA, &mB, &mC, resize_abc[0], resize_abc[1], resize_abc[2]);
  
  // Matzizeak GPU-ra eraman
  cudaMemcpy(m_A, mA, a*b*sizeof(float), cudaMemcpyHostToDevice);
  cudaMemcpy(m_B, mB, b*c*sizeof(float), cudaMemcpyHostToDevice);
  cudaMemcpy(m_C, mC, a*c*sizeof(float), cudaMemcpyHostToDevice);

  //printf("[+] HASIERAKO MATRIZEAK\n");
        //print_matrixes(a, b, c, &mA, &mB, &mC);

  // Tamainak datu mota egokian zehaztu
  dim3 threadsPerBlockd3(THREADS_PER_BLOCK, THREADS_PER_BLOCK);
  dim3 numBlocksd3(numBlocks, numBlocks);

  //Denbora neurtzen hasi
  cudaEventRecord(hasi);
  
  // Kernela deitu
  eragiketa<<<numBlocksd3,threadsPerBlockd3>>>(m_A, m_B, m_C, a, b, c);

  // Gelditu denbora neurketa
  cudaEventRecord(bukatu);
  cudaEventSynchronize(bukatu);

  cudaDeviceSynchronize();
  cudaMemcpy(mC, m_C, (a*c)*sizeof(float), cudaMemcpyDeviceToHost); // Honek datuak GPU-tik HOST-era ekartzen ditu

  //Kalkulatu denbora
  cudaEventElapsedTime(&gpu_denbora, hasi, bukatu);

  //printf("[+] BUKAERAKO MATRIZEAK\n");
        //print_matrixes(a, b, c, &mA, &mB, &mC);

  printf("BLOKE ZENBAKIA: %d | HARI ZENBAKIA BLOKEKO: %d }-> EXEKUZIO DENBORA: %10f\n", numBlocks, THREADS_PER_BLOCK, gpu_denbora);

  // Memoria askatu
        free(mA);
        free(mB);
        free(mC);
        
        free(resize_abc);

  cudaFree(m_A);
  cudaFree(m_B);
  cudaFree(m_C);

  // Aldagaiak askatu
  cudaEventDestroy(hasi);
  cudaEventDestroy(bukatu);

        return 0;
}
